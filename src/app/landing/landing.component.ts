import { Component, AfterViewInit } from '@angular/core';
import { WOW } from 'wowjs/dist/wow.min';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements AfterViewInit {

    constructor() {

    }

    ngAfterViewInit() {
        new WOW().init();
    }

    scroll(el) {
        el.scrollIntoView({behavior: 'smooth', block: 'start'});
    }


}
