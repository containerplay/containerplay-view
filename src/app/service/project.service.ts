import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ProjectService extends BaseService {

    private baseurl = BaseService.baseURL;
    constructor(private http: HttpClient) {
        super();
    }

    public saveProject(params: any, newProjectId: any): any {
        // tslint:disable-next-line:prefer-const
        let url = this.baseurl + 'projects';
        if ( newProjectId !== false ) {
            // url += '/' + newProjectId;
        }
        return this.http.post(url, params);
    }

    public getProjects(): any {
        const url = this.baseurl + 'projects';
        return this.http.get(url);
    }

}
