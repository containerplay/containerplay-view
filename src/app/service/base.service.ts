import { Injectable } from '@angular/core';

@Injectable()
export class BaseService {
    static baseURL: String = 'http://designer-rest-services-qa.apps.oc-cluster.anuragsdemo.club/v1/';
    constructor() { }

    public handleError(error: any): Promise<any> {
        return Promise.reject( error || error.message);
    }
}
