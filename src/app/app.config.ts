const CONFIG = {
N : 10,
LANGUAGES: [
    'Java',
    'MySQL',
    'HTML',
    'Node.js'
],
SPECTRUM: [
    'rgb(121,85,72)',
    'rgb(128,186,236)',
    'rgb(77,158,228)',
    'rgb(38,137,223)',
    'rgb(0,116,217)',
    'rgb(0,106,197)'
]
};


export default CONFIG;
