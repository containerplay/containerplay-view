
export class Tool {
    name: String;
    icon: String;
    type: String;
    status: boolean;
}
