import { Component, Renderer2 } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent {

    previousUrlBody: string;
    previousUrlHtml: string;

    constructor(private renderer: Renderer2, private router: Router) {
        this.router.events
            .subscribe((event) => {
            if ( event instanceof NavigationEnd ) {
                if ( this.previousUrlBody ) {
                    this.renderer.removeClass(document.body, this.previousUrlBody);
                    this.previousUrlBody = null;
                }

                if ( this.previousUrlHtml ) {
                    this.renderer.removeClass(document.documentElement, this.previousUrlHtml);
                    this.previousUrlHtml = null;
                }

                let currentUrlSlug = event.urlAfterRedirects.slice(1);
                currentUrlSlug = currentUrlSlug.split('/').join('-');

                this.previousUrlBody = currentUrlSlug;

                if ( currentUrlSlug ) {

                    switch ( currentUrlSlug ) {
                        case 'login':
                            this.previousUrlBody = 'login-pf-page';
                            this.previousUrlHtml = 'login-pf';
                            break;

                        case 'landing':
                            this.previousUrlBody = 'home';
                            this.previousUrlHtml = 'home-container';
                    }

                    this.renderer.addClass(document.body, this.previousUrlBody);

                    if ( this.previousUrlHtml ) {
                        this.renderer.addClass(document.documentElement, this.previousUrlHtml);
                    }
                }
            }
        });
    }
}
