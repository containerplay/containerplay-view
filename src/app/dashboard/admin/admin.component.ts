import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

    switchOptions = {
        onText: 'On',
        offText: 'Off',
        onColor: 'green',
        offColor: 'red',
        size: 'normal'
    };

    languages: any;
    databases: any;
    middlewares: any;
    jboss: any;
    openshifts: any;

    constructor() { }

    ngOnInit() {

        this.languages = [{
            name: 'Java',
            icon: '<img src="assets/img/icons/java.png">',
            type: 'java',
            status: true
        }, {
            name: 'JavaScript',
            icon: '<img src="assets/img/icons/js.svg">',
            type: 'javascript',
            status: true
        }, {
            name: 'Perl',
            icon: '<img src="assets/img/icons/perl.svg">',
            type: 'perl',
            status: true
        }, {
            name: 'Dot Net',
            icon: '<img src="assets/img/icons/dotnet.svg">',
            type: 'dotnet',
            status: true
        }, {
            name: 'PHP',
            icon: '<img src="assets/img/icons/php.png">',
            type: 'php',
            status: true
        }];

        this.databases = [{
            name: 'MongoDB',
            icon: '<img src="assets/img/icons/mongodb.svg">',
            type: 'mongodb',
            status: true
        }, {
            name: 'MySQL',
            icon: '<img src="assets/img/icons/mysql-database.svg">',
            type: 'mysql',
            status: true
        }, {
            name: 'Postgres SQL',
            icon: '<img src="assets/img/icons/postgresql.svg">',
            type: 'postgresql',
            status: true
        }];

        this.middlewares = [{
            name: 'Tomcat',
            icon: '<img src="assets/img/icons/tomcat.svg">',
            type: 'tomcat',
            status: true
        }, {
            name: 'WildFly',
            icon: '<img src="assets/img/icons/wildfly.svg">',
            type: 'wildfly',
            status: true
        }];

        this.jboss = [{
            name: 'Drools',
            icon: '<img src="assets/img/icons/drools_icon.svg">',
            type: 'drools',
            status: true
        }, {
            name: 'JDG',
            icon: '<img src="assets/img/icons/jdg.jpg">',
            type: 'jdg',
            status: true
        }];

        this.openshifts = [{
            name: 'Tool Node',
            icon: '<i class="fa pficon-container-node tool tool-node"></i>',
            type: 'tool-node',
            status: true
        }, {
            name: 'Tool Cubes',
            icon: '<i class="fa fa-cubes tool tool-cubes"></i>',
            type: 'tool-cubes',
            status: true
        }, {
            name: 'Tool Cube',
            icon: '<i class="fa fa-cube tool tool-cube"></i>',
            type: 'tool-cube',
            status: true
        }, {
            name: 'Tool Image',
            icon: '<i class="fa pficon-image tool tool-image"></i>',
            type: 'tool-image',
            status: true
        }, {
            name: 'Tool Replicator',
            icon: '<i class="fa pficon-replicator tool tool-replicator"></i>',
            type: 'tool-replicator',
            status: true
        }, {
            name: 'Tool Service',
            icon: '<i class="fa pficon-service tool tool-service"></i>',
            type: 'tool-service',
            status: true
        }, {
            name: 'Tool Route',
            icon: '<i class="fa pficon-route tool tool-route"></i>',
            type: 'tool-route',
            status: true
        }, {
            name: 'Tool Machine',
            icon: '<i class="fa pficon-virtual-machine tool tool-machine"></i>',
            type: 'tool-machine',
            status: true
        }];
    }

    onFlagChange(event) {
        console.log(event);
    }

}
