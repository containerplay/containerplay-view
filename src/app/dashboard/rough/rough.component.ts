import { Component } from '@angular/core';
import APP_CONFIG from '../../app.config';
import { Node, Link } from '../../d3';

@Component({
  selector: 'app-rough',
  templateUrl: './rough.component.html',
  styleUrls: ['./rough.component.scss']
})
export class RoughComponent  {

    nodes: Node[] = [];
    links: Link[] = [];

    constructor() {
      const N = APP_CONFIG.N,
            LANGUAGES = APP_CONFIG.LANGUAGES,
            getIndex = number => number - 1;

        
      /** constructing the nodes array */
      for (let i=1; i<=LANGUAGES.length; i++) {
        this.nodes.push(new Node(i, LANGUAGES[i-1]));
      }

      this.links.push(new Link(1, 2));
      this.links.push(new Link(1, 3));
      this.links.push(new Link(2, 3));
      this.links.push(new Link(3, 4));
      
    //   for (let i = 1; i <= N; i++) {
    //     for (let m = 2; i * m <= N; m++) {
    //       /** increasing connections toll on connecting nodes */
    //       this.nodes[getIndex(i)].linkCount++;
    //       this.nodes[getIndex(i * m)].linkCount++;

    //       /** connecting the nodes before starting the simulation */
    //       console.log('i = ' + i + ', m = ' + m);
    //       console.log(i, i * m);
          
    //       this.links.push(new Link(i, i * m));
    //     }
    //   }

    }

}
