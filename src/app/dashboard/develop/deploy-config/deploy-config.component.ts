import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-deploy-config',
  templateUrl: './deploy-config.component.html',
  styleUrls: ['./deploy-config.component.scss']
})
export class DeployConfigComponent implements OnInit {

    @Input() _formData: any;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onFormDataChange = new EventEmitter<boolean>();

    @Input()
        set formData(formData: any) {
        this._formData = formData;
    }

    deployConfigForm: FormGroup;

    constructor() { }

    ngOnInit() {
        this.deployConfigForm = new FormGroup({
            newimageisavailabler: new FormControl(),
            deploymentconfigurationchanges: new FormControl()
        });

        if ( this._formData !== null ) {
            this.deployConfigForm.setValue(this._formData);
        }

        this.onFormChanges();
    }

    onFormChanges(): void {
        this.deployConfigForm.valueChanges.subscribe(val => {
            this.onFormDataChange.emit(val);
        });
    }

    getData(): any {
        return this._formData;
    }

}
