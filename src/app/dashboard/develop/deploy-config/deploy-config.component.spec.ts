import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeployConfigComponent } from './deploy-config.component';

describe('DeployConfigComponent', () => {
  let component: DeployConfigComponent;
  let fixture: ComponentFixture<DeployConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeployConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeployConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
