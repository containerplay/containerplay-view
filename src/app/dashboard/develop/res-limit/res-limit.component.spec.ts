import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResLimitComponent } from './res-limit.component';

describe('ResLimitComponent', () => {
  let component: ResLimitComponent;
  let fixture: ComponentFixture<ResLimitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResLimitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResLimitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
