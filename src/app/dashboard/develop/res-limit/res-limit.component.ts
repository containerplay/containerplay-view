import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-res-limit',
  templateUrl: './res-limit.component.html',
  styleUrls: ['./res-limit.component.scss']
})
export class ResLimitComponent implements OnInit {

    @Input() _formData: any;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onFormDataChange = new EventEmitter<boolean>();

    @Input()
        set formData(formData: any) {
        this._formData = formData;
    }

    resLimitForm: FormGroup;

    constructor() { }

    ngOnInit() {
        this.resLimitForm = new FormGroup({
            requestInMillicores: new FormControl(),
            limitInMillicores: new FormControl(),
            requestInMiB: new FormControl(),
            limitInMiB: new FormControl()
        });

        if ( this._formData !== null ) {
            this.resLimitForm.setValue(this._formData);
        }

        this.onFormChanges();
    }

    onFormChanges(): void {
        this.resLimitForm.valueChanges.subscribe(val => {
            this.onFormDataChange.emit(val);
        });
    }

    getData(): any {
        return this._formData;
    }
}
