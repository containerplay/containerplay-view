import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-build-config',
  templateUrl: './build-config.component.html',
  styleUrls: ['./build-config.component.scss']
})
export class BuildConfigComponent implements OnInit {

    @Input() _formData: any;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onFormDataChange = new EventEmitter<boolean>();

    @Input()
        set formData(formData: any) {
        this._formData = formData;
    }

    buildConfigForm: FormGroup;

    constructor() { }

    ngOnInit() {
        this.buildConfigForm = new FormGroup({
            launchFirstBuild: new FormControl(),
            configureawebhookbuildtrigger: new FormControl(),
            automaticallybuildwhenbuilderimagechanges: new FormControl()
        });

        if ( this._formData !== null ) {
            this.buildConfigForm.setValue(this._formData);
        }

        this.onFormChanges();
    }

    onFormChanges(): void {
        this.buildConfigForm.valueChanges.subscribe(val => {
            this.onFormDataChange.emit(val);
        });
    }

    getData(): any {
        return this._formData;
    }

}
