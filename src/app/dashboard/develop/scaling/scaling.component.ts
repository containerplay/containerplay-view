import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-scaling',
  templateUrl: './scaling.component.html',
  styleUrls: ['./scaling.component.scss']
})
export class ScalingComponent implements OnInit {


    @Input() _formData: any;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onFormDataChange = new EventEmitter<boolean>();

    @Input()
        set formData(formData: any) {
        this._formData = formData;
    }

    scalingForm: FormGroup;

    constructor() { }

    ngOnInit() {
        this.scalingForm = new FormGroup({
            scalingStrategy: new FormControl(),
            scalingReplicas: new FormControl()
        });

        if ( this._formData !== null ) {
            this.scalingForm.setValue(this._formData);
        }

        this.onFormChanges();
    }

    onFormChanges(): void {
        this.scalingForm.valueChanges.subscribe(val => {
            this.onFormDataChange.emit(val);
        });
    }

    getData(): any {
        return this._formData;
    }

}
