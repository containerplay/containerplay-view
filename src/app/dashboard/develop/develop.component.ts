import { Component, OnInit, ElementRef, ViewChild, HostListener, TemplateRef, ViewEncapsulation } from '@angular/core';
import { DndDropEvent } from 'ngx-drag-drop';
import { debug } from 'util';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ProjectService } from '../../service/project.service';
import { Notification, NotificationService, NotificationType } from 'patternfly-ng';

export enum KEY_CODE {
    DLETE = 8,
    BACKSPACE = 46
}

@Component({
  selector: 'app-develop',
  templateUrl: './develop.component.html',
  styleUrls: ['./develop.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class DevelopComponent implements OnInit {

    @ViewChild('wrapper') wrapper: ElementRef;
    @ViewChild('sidebarFormTabs') sidebarFormTabs: ElementRef;

    saveProjectForm: FormGroup;

    notifications: Notification[];

    projectId: any;
    projectName: String;
    projectDescription: String;

    IconSize: any = {
        width: 50,
        height: 50
    };

    selectedItem: any;
    selectedLine: any;
    activeDragItem: any;
    activeDragLine: any;
    startEventTrack: any;
    sidebarScrollRight = false;
    sidebarOpen = false;

    languages: any;
    databases: any;
    middlewares: any;
    jboss: any;
    openshifts: any;

    graphicsIcon: any[] = [];   // array of icons created dynamically in runtime
    graphicsLine: any[] = [];   // array of lines created dynamically in runtime

    connectedLines: any[] = [];

    isLineDropVisible = true;
    modalRef: BsModalRef;

    saveProjectProgress = false;
    openProjectProgress = false;

    allProjects: any = [];

    constructor(private modalService: BsModalService,
                private projectService: ProjectService,
                private notificationService: NotificationService) {

        this.notifications = this.notificationService.getNotifications();
        notificationService.setDelay(5000); // set global notification delay time
    }


    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    // icon selection and deselection
    selectIcon (event, icon) {
        event.preventDefault();
        event.stopPropagation();
        this.selectedItem = null;
        this.selectedItem = icon;
        this.selectedLine = null;
        if ( icon == null ) {
            this.sidebarOpen = false;
        }
    }

    // icon selection and deselection
    selectLine (event, line) {
        event.preventDefault();
        event.stopPropagation();
        this.selectedLine = line;
        this.selectedItem = null;
        if ( line == null ) {
            this.sidebarOpen = false;
        }
    }

    // event listener for delete key across develop component
    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (event.keyCode === KEY_CODE.DLETE || event.keyCode === KEY_CODE.BACKSPACE) {
            if ( this.sidebarOpen === false ) {
                this.deleteItemConfirm();
            }
        }
    }

    // delete confirm dialog for selected line or icon
    deleteItemConfirm() {
        if ( this.selectedItem != null ) {
            swal({
                // title: 'Are you sure?',
                text: 'Are you sure you want to delete selected icon?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                // if confirmed by user
                if (result.value) {
                    this.removeIcon();
                }
            });
        } else if ( this.selectedLine != null ) {
            // 'Are you sure you want to delete selected icon?'
            swal({
                // title: 'Are you sure?',
                text: 'Are you sure you want to delete selected line?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                // if confirmed by user
                if (result.value) {
                    this.removeLine();
                }
            });
        }
    }

    // remove selected icon
    removeIcon() {
        let iconId = null;

        // remove all lines pointed to current sleected icon
        this.graphicsIcon.forEach((icon, index) => {
            if ( this.selectedItem === icon ) {
                iconId = icon.id;
                this.graphicsIcon.splice(index, 1);
            }
        });

        const remainingLines = [];
        this.graphicsLine.forEach((line, index) => {
            if ( !(line['line1'] === iconId || line['line2'] === iconId) ) {
                remainingLines.push(line);
            }
        });
        this.graphicsLine = remainingLines;
        this.selectedItem = null;
    }

    // remove selected line
    removeLine() {
        this.graphicsLine.forEach((line, index) => {
            if ( this.selectedLine === line ) {
                this.graphicsLine.splice(index, 1);
            }
        });
        this.selectedLine = null;
    }

    // clear all selection and active tracking
    clear(data: any) {
        switch ( data ) {
            case 'icons':
                this.graphicsIcon = [];
                break;

            case 'lines':
                this.graphicsLine = [];
                break;

            default:
                this.graphicsLine = [];
                this.graphicsIcon = [];
        }
        this.selectedItem = this.selectedLine = this.activeDragItem = this.activeDragLine = this.startEventTrack = null;
        this.sidebarOpen = false;
    }

    /*
    * executed once when user starts to drag icon
    * keep the tracking coordinates for starting point
    * keep icon id and reference
    */
    onDragStart(icon: any, event: DragEvent) {
        event.dataTransfer.dropEffect = 'move';
        this.activeDragItem = icon;
        this.startEventTrack = event;
        const itemEvent: any = event;
        event.dataTransfer.setDragImage(itemEvent.currentTarget, 25, 25);
        const iconId = icon.id;
        this.connectedLines = [];
        this.graphicsLine.forEach((line) => {
            if ( line['line1'] === iconId || line['line2'] === iconId ) {
                this.connectedLines.push(line);
            }
        });
    }

    /*
    * executed once when user starts to draw line
    * keep the tracking coordinates for starting point
    * add temproary line to visualize line
    */
    onDragStartLine(icon: any, event: DragEvent) {
        this.activeDragItem = icon;
        this.startEventTrack = event;
        this.isLineDropVisible = false;
        this.activeDragLine = {
            x1: icon.posX + this.IconSize.centerX,
            y1: icon.posY + this.IconSize.centerY,

            x2: icon.posX + this.IconSize.centerX,
            y2: icon.posY + this.IconSize.centerY,

            type: 'line'
        };
        this.graphicsLine.push(this.activeDragLine);
    }

    // remvoe temproary line
    onDragEndLine(icon: any, event: DragEvent) {
        this.graphicsLine.pop();
        this.isLineDropVisible = true;
    }

    // reset tracking coordinates after drag event completes
    onDragEnd(icon: any, event: DragEvent) {
        this.connectedLines = [];
        this.activeDragItem = null;
        this.activeDragLine = null;
        this.startEventTrack = null;
    }

    onDraggableMoved(icon, event: DragEvent) {
        // console.log('draggable moved', JSON.stringify(event, null, 2));
        // console.log(icon);
        // const boxPos = this.wrapper.nativeElement.getBoundingClientRect();

        // icon['posX'] =  event.x - boxPos.x - event.offsetX;
        // icon['posY'] = event.y - boxPos.y - event.offsetY;
    }

    onDragCanceled(event: DragEvent) {
        console.log('drag cancelled', JSON.stringify(event, null, 2));
    }

    // line, icon set end position while dragging
    onDragover(event: DragEvent) {
        this.onDragoverLines(event);

        if ( this.connectedLines.length !== 0 ) {
            this.connectedLines.forEach((line) => {
                const flag = ( this.activeDragItem['id'] === line['line1'] ) ? '1' : '2';
                line['x' + flag] = this.activeDragItem['posX'] + this.IconSize['centerX']  - this.startEventTrack.clientX + event.clientX ;
                line['y' + flag] = this.activeDragItem['posY'] + this.IconSize['centerX']  - this.startEventTrack.clientY + event.clientY;
            });
        }
    }

    // line set end position while dragging
    onDragoverLines(event: DragEvent) {
        if ( this.activeDragLine != null && this.activeDragLine !== '') {
            this.activeDragLine.x2 =  - this.startEventTrack.clientX + event.clientX + this.activeDragItem.posX + this.IconSize.centerX;
            this.activeDragLine.y2 = - this.startEventTrack.clientY + event.clientY + this.activeDragItem.posY + this.IconSize.centerY;
        }
    }

    // for icons and temproary lines
    onDrop(event: DndDropEvent) {
        const data = JSON.stringify(event, null, 2);

        const boxPos = this.wrapper.nativeElement.getBoundingClientRect();
        const iconData = event.data;
        const e = event.event;
        if ( iconData !== null ) {

            if ( typeof iconData.type !== 'undefined' && iconData.type === 'icon') {
                // script for icons which is already present in stage

                this.activeDragItem['posX'] +=  e.clientX - this.startEventTrack.clientX - 2;
                this.activeDragItem['posY'] +=  e.clientY - this.startEventTrack.clientY - 2;
                console.log(iconData);
                return false;

            } else if (iconData.type === 'line') {
                // ignore for line

            } else {

                // add icon on stage
                iconData['posX'] =  e.x - boxPos.x - this.IconSize.centerX;
                iconData['posY'] = e.y - boxPos.y - this.IconSize.centerY;
                iconData['effectAllowed'] = 'move';
                iconData['group'] = iconData.type;
                iconData['type'] = 'icon';
                iconData['id'] = 'icon-' + Date.now();
                iconData['form'] = {
                    container: null,
                    buildconfig: null,
                    labels: null,
                    deployconfig: null,
                    scaling: null,
                    reslimit: null
                };
                this.graphicsIcon.push(iconData);
            }
        }
    }

    // for lines droppable and connection building
    onDropLine(icon: any, event: DndDropEvent) {

        if ( icon.group !== this.activeDragItem.group ) {

            const linePos = {
                x1: this.activeDragLine.x1,
                y1: this.activeDragLine.y1,
                x2: icon.posX + this.IconSize.centerX,
                y2: icon.posY + this.IconSize.centerY,
                type: 'line',
                id: 'line-' + Date.now(), // time stamp => for unique id
                line1: this.activeDragItem.id,
                line2: icon.id
            };

            // insert line at 2nd last pos
            setTimeout(() => {
                this.graphicsLine.push(linePos);
            }, 100);

        } else {

            this.notificationService.message(
                NotificationType.WARNING,
                'Warning',
                'Same type of technology cannot be connected.',
                false,
                null,
                null
            );
        }

        // reset all selection
        this.activeDragLine = null;
        this.activeDragItem = null;
        this.startEventTrack = null;
    }

    getStdForm(): any {
        let containerForm: FormGroup;
        containerForm = new FormGroup({
            containerName: new FormControl(),
            gitRepoURL: new FormControl(),
            gitRepoReference: new FormControl(),
            contextDir: new FormControl(),
            sourceSecret: new FormControl()
        });

        const form = {
            container: containerForm
        };

        return form;
    }

    sideBarNavScrollRight() {
        this.sidebarScrollRight = true;
    }

    sideBarNavScrollLeft() {
        this.sidebarScrollRight = false;
    }

    openSidebar() {
        this.sidebarOpen = !this.sidebarOpen;
    }

    formDataChange(formtype, formData) {
        this.selectedItem.form[formtype] = formData;
    }

    clearStage() {
        this.clear('all');
    }

    // validate and save project to server
    saveProject(pData: any) {
        console.log(pData);
        let isNewProject = false;
        const jsonObject = {
            lines: this.graphicsLine,
            icons: this.graphicsIcon
        };

        const params: any = {
            projectJsonHTML: JSON.stringify(jsonObject),
            name: pData.projectName,
            description: pData.projectDescription,
            updatedAt: Date.now()
        };

        if ( this.projectId !== null && this.projectId !== undefined ) {
            isNewProject = this.projectId;
        }

        this.saveProjectProgress = true;
        this.projectService.saveProject(params, isNewProject).subscribe(res => {
            this.saveProjectProgress = false;
            if ( res == null ) {
                console.log('Project saved sucessfully');
                this.modalRef.hide();
                this.notificationService.message(
                    NotificationType.SUCCESS,
                    'Success',
                    'Project saved sucessfully.',
                    false,
                    null,
                    null
                );

                if ( this.projectId !== null && this.projectId !== undefined ) {
                    this.clearStage();
                }
            } else {
                this.notificationService.message(
                    NotificationType.DANGER,
                    'Error',
                    'There was an error saving the project. Please try again.',
                    false,
                    null,
                    null
                );
            }
        });

    }

    // get all projects list
    openProjects() {
        this.openProjectProgress = true;
        this.projectService.getProjects().subscribe(res => {
            this.openProjectProgress = false;
            if ( res.length > 0 ) {
                this.allProjects = res;
            } else {
                this.allProjects = [];
            }
        });
    }

    // save project if already an existing project
    checkForOldProject() {
        if ( this.projectId !== null && this.projectId !== undefined ) {
            const userParams = {
                projectName: this.projectName,
                projectDescription: this.projectDescription
            };

            this.saveProjectForm.setValue(userParams);
        }
    }

    // show project data into stage
    renderProject(project: any): any {

        this.clearStage();

        this.projectId = project.id;
        this.projectName = project.name;
        this.projectDescription = project.description;

        if ( project.projectJsonHTML !== null ) {
            const json = JSON.parse(project.projectJsonHTML);
            if ( json.lines !== null && json.lines !== undefined ) {
                this.graphicsLine = json.lines;
            }
            if ( json.icons !== null &&  json.icons !== undefined ) {
                this.graphicsIcon = json.icons;
            }
        }

        this.modalRef.hide();
        this.notificationService.message(
            NotificationType.SUCCESS,
            'Success',
            '' + project.name + ' opened.',
            false,
            null,
            null
        );
    }

    ngOnInit() {

        this.IconSize['centerX'] = this.IconSize.width / 2;
        this.IconSize['centerY'] = this.IconSize.width / 2;

        this.languages = [{
            name: 'Java',
            icon: '<img src="assets/img/icons/java.png">',
            type: 'language',
            status: true
        }, {
            name: 'JavaScript',
            icon: '<img src="assets/img/icons/js.svg">',
            type: 'language',
            status: true
        }, {
            name: 'Perl',
            icon: '<img src="assets/img/icons/perl.svg">',
            type: 'language',
            status: true
        }, {
            name: 'Dot Net',
            icon: '<img src="assets/img/icons/dotnet.svg">',
            type: 'language',
            status: true
        }, {
            name: 'PHP',
            icon: '<img src="assets/img/icons/php.png">',
            type: 'language',
            status: true
        }];

        this.databases = [{
            name: 'MongoDB',
            icon: '<img src="assets/img/icons/mongodb.svg">',
            type: 'database',
            status: true
        }, {
            name: 'MySQL',
            icon: '<img src="assets/img/icons/mysql-database.svg">',
            type: 'database',
            status: true
        }, {
            name: 'Postgres SQL',
            icon: '<img src="assets/img/icons/postgresql.svg">',
            type: 'database',
            status: true
        }];

        this.middlewares = [{
            name: 'Tomcat',
            icon: '<img src="assets/img/icons/tomcat.svg">',
            type: 'middleware',
            status: true
        }, {
            name: 'WildFly',
            icon: '<img src="assets/img/icons/wildfly.svg">',
            type: 'middleware',
            status: true
        }];

        this.jboss = [{
            name: 'Drools',
            icon: '<img src="assets/img/icons/drools_icon.svg">',
            type: 'jboss',
            status: true
        }, {
            name: 'JDG',
            icon: '<img src="assets/img/icons/jdg.jpg">',
            type: 'jboss',
            status: true
        }];

        this.openshifts = [{
            name: 'Tool Node',
            icon: '<i class="fa pficon-container-node tool tool-node"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Cubes',
            icon: '<i class="fa fa-cubes tool tool-cubes"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Cube',
            icon: '<i class="fa fa-cube tool tool-cube"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Image',
            icon: '<i class="fa pficon-image tool tool-image"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Replicator',
            icon: '<i class="fa pficon-replicator tool tool-replicator"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Service',
            icon: '<i class="fa pficon-service tool tool-service"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Route',
            icon: '<i class="fa pficon-route tool tool-route"></i>',
            type: 'openshift',
            status: true
        }, {
            name: 'Tool Machine',
            icon: '<i class="fa pficon-virtual-machine tool tool-machine"></i>',
            type: 'openshift',
            status: true
        }];

        this.saveProjectForm = new FormGroup({
            projectName: new FormControl('', {
                validators: Validators.required
            }),
            projectDescription: new FormControl('', {
                validators: Validators.required
            })
        });

    }


}
