import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-labels',
  templateUrl: './labels.component.html',
  styleUrls: ['./labels.component.scss']
})
export class LabelsComponent implements OnInit {

    @Input() _formData: any;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onFormDataChange = new EventEmitter<boolean>();

    @Input()
        set formData(formData: any) {
        this._formData = formData;
    }

    labelsForm: FormGroup;

    constructor() { }

    ngOnInit() {
        this.labelsForm = new FormGroup({
            containerName: new FormControl(),
            containerGiturl: new FormControl()
        });

        if ( this._formData !== null ) {
            this.labelsForm.setValue(this._formData);
        }

        this.onFormChanges();
    }

    onFormChanges(): void {
        this.labelsForm.valueChanges.subscribe(val => {
            this.onFormDataChange.emit(val);
        });
    }

    getData(): any {
        return this._formData;
    }

}
