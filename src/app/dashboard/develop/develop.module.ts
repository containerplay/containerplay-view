import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { DevelopComponent } from './develop.component';
import { RouterModule } from '@angular/router';

import { NavigationModule, CardModule, NotificationModule } from 'patternfly-ng';
import { BsDropdownModule, AccordionModule } from 'ngx-bootstrap';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { DndModule } from 'ngx-drag-drop';
import { TabsModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContainerFormComponent } from './container-form/container-form.component';
import { BuildConfigComponent } from './build-config/build-config.component';
import { LabelsComponent } from './labels/labels.component';
import { DeployConfigComponent } from './deploy-config/deploy-config.component';
import { ScalingComponent } from './scaling/scaling.component';
import { ResLimitComponent } from './res-limit/res-limit.component';
import { ModalModule } from 'ngx-bootstrap';
import { ProjectService } from '../../service/project.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
        {path: '', component: DevelopComponent, pathMatch: 'full'}
    ]),
    HttpClientModule,
    NavigationModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    CardModule,
    BootstrapSwitchModule,
    DndModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    NotificationModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    DevelopComponent,
    ContainerFormComponent,
    BuildConfigComponent,
    LabelsComponent,
    DeployConfigComponent,
    ScalingComponent,
    ResLimitComponent
  ],
  providers: [ProjectService]
})

export class DevelopModule { }
