import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-container-form',
  templateUrl: './container-form.component.html',
  styleUrls: ['./container-form.component.scss']
})
export class ContainerFormComponent implements OnInit {

    @Input() _formData: any;
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onFormDataChange = new EventEmitter<boolean>();

    @Input()
        set formData(formData: any) {
        this._formData = formData;
    }

    containerForm: FormGroup;

    constructor() { }

    ngOnInit() {
        this.containerForm = new FormGroup({
            containerName: new FormControl(),
            gitRepoURL: new FormControl(),
            gitRepoReference: new FormControl(),
            contextDir: new FormControl(),
            sourceSecret: new FormControl()
        });

        if ( this._formData !== null ) {
            this.containerForm.setValue(this._formData);
        }

        this.onFormChanges();
    }

    onFormChanges(): void {
        this.containerForm.valueChanges.subscribe(val => {
            this.onFormDataChange.emit(val);
        });
    }

    getData(): any {
        return this._formData;
    }

}
