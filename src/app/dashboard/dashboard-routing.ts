import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
// import { DevelopComponent } from './develop/develop.component';
import { ExploreComponent } from './explore/explore.component';
import { ManageComponent } from './manage/manage.component';
import { ReportingComponent } from './reporting/reporting.component';
import { AdminComponent } from './admin/admin.component';
import { LearnComponent } from './learn/learn.component';
import { SupportComponent } from './support/support.component';
import { RoughComponent } from './rough/rough.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'explore', component: ExploreComponent },
          { path: 'manage', component: ManageComponent },
          { path: 'reporting', component: ReportingComponent },
          { path: 'admin', component: AdminComponent},
          { path: 'learn', component: LearnComponent },
          { path: 'support', component: SupportComponent },
          { path: 'rough', component: RoughComponent },
          { path: '', component: ExploreComponent },
          { path: 'develop', loadChildren: 'app/dashboard/develop/develop.module#DevelopModule' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule {}

