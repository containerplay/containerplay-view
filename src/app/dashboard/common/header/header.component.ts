import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Action } from 'patternfly-ng';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() navToggle = new EventEmitter<boolean>();

  constructor() {

  }

  ngOnInit() {
  }

  toggleSidebar() {
    this.navToggle.emit(true);
  }


}
