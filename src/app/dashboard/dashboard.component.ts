import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationItemConfig } from 'patternfly-ng';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit  {
  @ViewChild('sidenav') public sidenav;

  navigationItems: NavigationItemConfig[];
  actionText = '';

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.navigationItems = [
      {
        title: 'Explore',
        iconStyleClass: 'fa fa-dashboard',
        url: '/dashboard/explore'
      },
      {
        title: 'Develop',
        iconStyleClass: 'fa fa-shield',
        url: '/dashboard/develop'
      },
      {
        title: 'Manage',
        iconStyleClass: 'fa fa-space-shuttle',
        url: '/dashboard/manage'
      },
      {
        title: 'Reporting',
        iconStyleClass: 'fa fa-paper-plane',
        url: '/dashboard/reporting'
      },
      {
        title: 'Admin',
        iconStyleClass: 'fa fa-graduation-cap',
        url: '/dashboard/admin'
      },
      {
        title: 'Learn',
        iconStyleClass: 'fa fa-gamepad',
        url: '/dashboard/learn'
      },
      {
        title: 'Support',
        iconStyleClass: 'fa fa-phone',
        url: '/dashboard/support'
      }

    ];
  }

  onItemClicked($event: NavigationItemConfig): void {
    this.actionText += 'Item Clicked: ' + $event.title + '\n';
  }

  onNavigation($event: NavigationItemConfig): void {
    this.actionText += 'Navigation event fired: ' + $event.title + '\n';
  }
}
