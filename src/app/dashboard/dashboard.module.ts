import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing';

import { SidebarComponent } from './common/sidebar/sidebar.component';
import { HeaderComponent } from './common/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { ExploreComponent } from './explore/explore.component';
import { ManageComponent } from './manage/manage.component';
import { ReportingComponent } from './reporting/reporting.component';
import { AdminComponent } from './admin/admin.component';
import { LearnComponent } from './learn/learn.component';
import { SupportComponent } from './support/support.component';
import { FormsModule } from '@angular/forms';

import { NavigationModule, CardModule } from 'patternfly-ng';
import { BsDropdownModule, AccordionModule } from 'ngx-bootstrap';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { RoughComponent } from './rough/rough.component';

import { D3Service, D3_DIRECTIVES } from '../d3';
import { GraphComponent } from '../visuals/graph/graph.component';
import { SHARED_VISUALS } from '../visuals/shared';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule,
    NavigationModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    CardModule,
    BootstrapSwitchModule,
    FormsModule
  ],
  declarations: [
    DashboardComponent,
    SidebarComponent,
    HeaderComponent,
    ExploreComponent,
    ManageComponent,
    ReportingComponent,
    AdminComponent,
    LearnComponent,
    SupportComponent,
    RoughComponent,
    GraphComponent,
    ...SHARED_VISUALS,
    ...D3_DIRECTIVES
  ],
  providers: [D3Service]

})
export class DashboardModule { }
